\section{Filter Design}
\label{sec:SLAM}

\subsection{Extended Kalman Filter}
Let the following nonlinear system, represents the mobile robot pose model dynamic expressed in~\eqref{eq:DTsystemUni}:

\begin{equation}
  \begin{aligned}
    X_{k+1}  &= f_k(\hat{X}_{k}, u_{k}, \gamma_{k})\\
    z_{k} &=  h(\hat X_{k})  + \varepsilon_{k},
  \end{aligned}
\label{eq:Predstate}
\end{equation}


where $u_k = [v_k,\omega_k]^T$ is the vector of the model velocities, $\gamma_k$ is the noise affecting the model velocities estimates given the odometry and $\varepsilon_{k}$ is the measurement uncertainty, with the covarainces $Q_k$ and $R_k$, respectively.  In a standard EKF, the predicted model uncertainty with the respective Jacobians is calculated as :
\begin{equation}
  \begin{aligned}
    P^-_{k+1}  &= A_kP_{k}A_k^T+B_kQ_{k}B_k^T ,\\
  \end{aligned}
\label{eq:PredCov}
\end{equation}

where the superscript notation is used to denote the predicted estimates given the model.  Of course, the initial value for the state is $X_0$, while the initial covariance is $P_0$. Hence, the EKF equations for the estimates updates are as follows: 
\begin{equation}
  \begin{aligned}
   K_{k}  &= {P}^-_kH^T_k(H_k{P}^-_kH^T_k+R_k)^{-1}, \\
   \hat{X}_{k} &= \hat{X}^-_{k} + K_k(z_k- h(\hat{X}^-_{k})) , \\
    P_{k}  &= (I-K_kH_k)P^-_k ,\\
  \end{aligned}
\label{eq:bayes2}
\end{equation}
where $K_{k}$ is the Kalman gain, $H_k$ is the Jacobian of the
measurement function.


\subsection{The Method}
As discussed earlier, without good initializations, BiLSTM neural networks fail to predict. Hence, we use the pose estimated values by the EKF for every sequence as the initial rough estimations to begin with. As we are assumed to have no knowledge about the noise stochastic features in advance, the estimation of robot pose provided by the EKF is highly unreliable. However, HMMs, due to the statistical inference, are able to maintain tractable inference. Now, we use BiLSTM to track these inferences, and memorize long term dependencies using the memory cell. \\


In order to be able to use a BiLSTM network, we must design a sequence model that has access to both past and future input features for a given time.  Assuming that the ranging
system is sampled with period $T_s$, at each time step $k T_s$,
$k\in\N$, the vehicle receives a ranging measure as follows: 
\begin{equation}
  O_k = \begin{bmatrix}
    o_{1,\,k}\\
    o_{2,\,k}\\
    \ldots\\
    o_{M,\,k}
\end{bmatrix} = \begin{bmatrix}
    \sqrt{\left(x_k-A_{1,x}\right)^2+\left(y_k-A_{1,y}\right)^2}\\
    \sqrt{\left(x_k-A_{2,x}\right)^2+\left(y_k-A_{2,y}\right)^2}\\
    \ldots\\
    \sqrt{\left(x_k-A_{M,x}\right)^2+\left(y_k-A_{M,x}\right)^2}
\end{bmatrix} .
\label{eq:OutputFuncDisc}
\end{equation}

where $A$ denotes the anchor located at known coordinates $x, \,y$. We then need to constitute a training dataset for our sequence model consisting training input and output sequences as follows :\\

$s_{in}^{(i)} = [O_{k+1,k+2}^-, \, \widehat{O}_{k+1,k+2}, \, O_{k+1,k+2},  \, a_{k+1,k+2}$,

$\qquad \qquad \qquad \qquad \qquad \qquad \qquad {O}_{k-1,k}^b, \, a_{k-1,k},\,  X_{k-1,k}^b$]\\

$s_{out}^{(i)} = {O}_{k+1,k+2}^{desired}$\\

where the superscript notations ``\textit{-}'' stand for the estimated value by ~\eqref{eq:Predstate}, ``\textit{$\widehat{-}$}'' stand for the estimated value by EKF and ``\textit{b}'' stand for our belief, the final estimation values by the filter. Other terms are the values obtained by the sensors. Then we train bidirectional LSTM networks using backpropagation through time (BPTT) ~\cite{boden2002guide}. Hence, in a manner similar to PSR, the model tries to predict the true system dynamics using the new action and observation taken by the robot at time $k$ conditioned on the history of observation and action. The length of the history and future sequences in the current implementation is two. Of course we can have arbitrarily long sequences depending on how much delay we want to have at the beginning. (\dan{** Remark : It might be possible to calculate the optimum value for the length of the history and future sequences})\\


In the localization phase, input and output sequences of the network will be as follows:\\

$s_{in,k} = [O_{k,k+1}^-, \, \widehat{O}_{k,k+1}, \, O_{k,k+1},  \, a_{k,k+1}$,

$\qquad \qquad \qquad \qquad \qquad  {O}_{k-2,k-1}^b, \, a_{k-2,k-1},\,  X_{k-2,k-1}^b$]\\

$s_{out,k} = {O}_{k+1}^{b}$\\

Finally, the Cartesian coordinates $(x_{k+1}^b, y_{k+1}^b)$ of the robot in the initial position can easily be found using at least three predicted observations. Subtracting ${{o}_{2,k+1}^b} ^2$ and ${{o}_{3,k+1}^b} ^2$ to ${{o}_{1,k+1}^b} ^2$, we get
\begin{equation}
  \label{eq:ObsvXY}
  \Sigma \begin{bmatrix} x_{k+1}^b \\ y_{k+1}^b \end{bmatrix} =\frac{1}{2}
  \begin{bmatrix}
    {{o}_{1,k+1}^b} ^2- {{o}_{2,k+1}^b} ^2 - A_{1,x}^2 -A_{1,y}^2 + A_{2,x}^2 + A_{2,y}^2\\
    {{o}_{1,k+1}^b} ^2- {{o}_{3,k+1}^b} ^2 - A_{1,x}^2 -A_{1,y}^2 + A_{3,x}^2 + A_{3,y}^2
  \end{bmatrix} ,
\end{equation}
where
\[
  \Sigma = \begin{bmatrix}
    A_{2,x} - A_{1,x} & A_{2,y} - A_{1,y}\\
    A_{3,x} - A_{1,x} & A_{3,y} - A_{1,y}
  \end{bmatrix} ,
\]
which yields the two unknowns $(x_{k+1}^b, y_{k+1}^b)$ since $\Sigma$ is
invertible if the sensors are not collinear.

