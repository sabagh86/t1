\section{Preliminary results}
\label{sec:results}

\begin{figure}
  \includegraphics[width=\linewidth]{Figures/Map.pdf}
  \caption{The simulated environment used for localization.}
  \label{fig:Map}
\end{figure}



The performance of the BiLSTM-EKF estimator for localization in known environments is investigated and compared with the classical EKF through a series of simulations. In order to validate the accuracy of the proposed method we carried out a Monte Carlo analysis with 1024 trials, i.e. 256 trials for four different trajectories. All of the simulations of this section are conducted in the environment shown in Figure~\ref{fig:Map} using the minimum number of range only anchors (i.e. three) thus resulting in a lower bound to the achievable performance. Of course all the uncertainties (i.e. $\sigma_v$ and $\sigma_{\omega}$ acting on the relative velocities, and $\sigma_r$ acting on the ranges) are assumed uniformly distributed. Monte Carlo executions, have $Q_k = \mbox{diag}(\sigma_v^2, \sigma_{\omega}^2)$, where $I_M$ is the identity matrix of dimension $M$. In all cases, a relatively fair initial configuration is assumed, so that for the initial pose vectors $X_0$ a uniformly distributed random noise vector with zero mean and 0.1 standard deviation is added to the actual start pose vector for both EKF and BiLSTM-EKF. The knowledge of the initial condition uncertainties are also assumed to be known which obviously means having the initial covariance $P_0 = \mbox{diag}(0.1, 0.1,0.1)$. The RMSE for position and orientation along with the localization results for the BiLSTM-EKF and classical EKF are reported in Figures~\ref{fig:RMSETrajectory1}~-~\ref{fig:RMSETrajectory4} in different scenarios. For giving a more accurate sense of the localization performance, as can be seen from the corresponding RMSE plots in the figures, the uncertainty values for the linear velocity $\sigma_v$ range from 0.05~m/s to 0.2~m/s, for the angular velocity $\sigma_{\omega}$ range from 0.05~rad/s to 0.2~rad/s and for the range reading is between 0.05~m to 0.2~m . The actual path (ground truth) followed by the robot in the Figures in the $X_w-Y_w$ plane is the thick solid red line, and the path generated by the EKF and BiLSTM-EKF are the green and the blue sets of lines, respectively. RMSE of the position error and of the orientation error of the vehicle are calculated as follows:

\[
  e_p^i = \sqrt{\frac{\sum_{k=1}^{N} (\hat x_k - x_k)^2 +  (\hat y_k - y_k)^2}{N}},
\]
and
\[
  e_\theta^i = \sqrt{\frac{\sum_{k=1}^{N} (\hat \theta_k - \theta_k)^2}{N}} ,
\]

\begin{figure}[!h]
  \includegraphics[width=\linewidth]{Figures/RMSETrajectory1.pdf}
  \caption{RMSE and the corresponding estimated trajectories by EKF (the left side images) and BiLSTM-EKF (the right side images) based on three anchors reported in the Figure over
$97$ Monte Carlo runs. The thick solid red line is the actual robot trajectory.}
  \label{fig:RMSETrajectory1}
\end{figure}
\begin{figure}[!h]
  \includegraphics[width=\linewidth]{Figures/RMSETrajectory2.pdf}
  \caption{RMSE and the corresponding estimated trajectories by EKF (the left side images) and BiLSTM-EKF (the right side images) based on three anchors reported in the Figure over
$83$ Monte Carlo runs. The thick solid red line is the actual robot trajectory.}
  \label{fig:RMSETrajectory2}
\end{figure}
\begin{figure}[!h]
  \includegraphics[width=\columnwidth]{Figures/RMSETrajectory3.pdf}
  \caption{RMSE and the corresponding estimated trajectories by EKF (the left side images) and BiLSTM-EKF (the right side images) based on three anchors reported in the Figure over
$130$ Monte Carlo runs. The thick solid red line is the actual robot trajectory.}
  \label{fig:RMSETrajectory3}
\end{figure}

\begin{figure}[!h]
  \includegraphics[width=\columnwidth]{Figures/RMSETrajectory4.pdf}
  \caption{RMSE and the corresponding estimated trajectories by EKF (the left side images) and BiLSTM-EKF (the right side images) based on three anchors reported in the Figure over
$97$ Monte Carlo runs. The thick solid red line is the actual robot trajectory.}
  \label{fig:RMSETrajectory4}
\end{figure}
For the localization result of the trajectory in Figure~\ref{fig:RMSETrajectory1}, the system evolution was simulated for 19.4 s (i.e. 97 $\times$ 0.2) with a sampling time Ts = 200 ms. The minimum, average, and maximum values for the linear velocity are equal to 1.5250~m/s, 1.9898~m/s, 2.6038~m/s, respectively. Also the corresponding values for the minimum, average, and maximum angular velocity are equal to -1.2985~rad/s, 0.2754~rad/s, 1.4998~rad/s.


For the localization result of the trajectory in Figure~\ref{fig:RMSETrajectory2}, the system evolution was simulated for 16.6 s (i.e. 83 $\times$ 0.2) with a sampling time Ts = 200 ms. The minimum, average, and maximum values for the linear velocity are equal to 1.5229~m/s, 1.9819~m/s, 2.4567~m/s, respectively. Also the corresponding values for the minimum, average, and maximum angular velocity are equal to -1.4151~rad/s, -0.1309~rad/s, 1.4151~rad/s.


\begin{figure}
  \includegraphics[width=\columnwidth]{Figures/SingleTrajectory.pdf}
  \caption{Path followed by the robot on the plane (solid line), estimated by the BiLSTM-EKF (blue line) and from the EKF (green line). All the noises $\sigma_v$ and $\sigma_{\omega}$, and $\sigma_r$ are assumed uniformly distributed.}
  \label{fig:SingleTrajectory}
\end{figure}
\begin{figure}
  \includegraphics[width=\columnwidth]{Figures/BiLSTMRMSE.pdf}
  \caption{Performance plots of the BiLSTM neural network in the first epoch. RMSE and loss of the training and validation sets are represented by the solid and dashed lines, respectively.}
  \label{fig:BiLSTMRMSE}
\end{figure}

For the localization result of the trajectory in Figure~\ref{fig:RMSETrajectory3}, the system evolution was simulated for 26 s (i.e. 130 $\times$ 0.2) with a sampling time Ts = 200 ms. The minimum, average, and maximum values for the linear velocity are equal to 1.2533~m/s, 2.0026~m/s, 2.5766~m/s, respectively. Also the corresponding values for the minimum, average, and maximum angular velocity are equal to -1.3602~rad/s, 0.2058~rad/s, 1.4648~rad/s.


For the localization result of the trajectory in Figure~\ref{fig:RMSETrajectory4}, the system evolution was simulated for 19.4 s (i.e. 97 $\times$ 0.2) with a sampling time Ts = 200 ms. The minimum, average, and maximum values for the linear velocity are equal to 1.4849~m/s, 1.9699~m/s, 2.4444~m/s, respectively. Also the corresponding values for the minimum, average, and maximum angular velocity are equal to -1.3768~rad/s, 0.1829~rad/s, 1.5447~rad/s.\\

Figure~\ref{fig:SingleTrajectory} depicts the localization result for a random trajectory in the environment shown in Figure~\ref{fig:Map}, where uniformly distributed random noises are acting on the velocities and range measurements. In this case, the corresponding uncertainty values are as $\sigma_v = 0.1$~m/s, $\sigma_{\omega} = 0.1$~rad/s and $\sigma_r = 0.1$~m/s. The actual path (ground truth) followed by the robot in the Figures in the $X_w-Y_w$ plane is the thick solid red line, and the path generated by the EKF and BiLSTM-EKF are the green and the blue line, respectively. The system evolution was simulated for 11.4 s with a sampling time Ts = 200 ms. The resulted trajectories well prove the performance of the presented BiLSTM-EKF design in presence of unknown uncertainties.




Finally the last part of the simulation is the performance of BiLSTM neural networks on the localization dataset consisting 168000 sequences for training, 36000 sequences for validation, and 36000 sequences for test. These sequences are randomly generated through 1200 tour taken by the robot in the environment shown in Figure~\ref{fig:Map}. The resulted loss and RMSE for one epoch of the training process are depicted in Figure~\ref{fig:BiLSTMRMSE} . As can be seen from the graphs, the training and validation RMSE quickly declines to around zero and become 0.2323 at end of the first epoch. The final validation RMSE at the final epoch (i.e. 20) is around 0.03 which shows the effectiveness of the proposed strategy.\\\\\\\\\\\\\\\\\\\\

\dan{**Remark : it is worth mentioning that the localization results (not the BiLSTM training performance) are the minimum accuracy of the system, because now the size of the dataset (i.e. 240000 samples) is very small for approximating such strongly nonlinear dynamics. For achieving the best results, a sensibly more number of sequences are required. I must tries different depth architecture for the network and after finalizing the approach, I will train the network with way more number of sequences}
% All of the results have demonstrated a considerable
% improvement for the smoother estimators over the standard EKF in
% overall. However, these figures also clearly show that the six-state
% parameterisation yields an inconsistent covariance.




